{
 "cells": [
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "# Overfitting and Underfitting",
   "id": "168a953caec09874"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "In this exercise, we will learn how to improve training outcomes by including an early\n",
    "stopping callback to prevent overfitting."
   ],
   "id": "deeab69cc62a0b80"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Imports",
   "id": "3b049dd7474b9c27"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "from pandas import DataFrame\n",
    "from pandas import read_csv\n",
    "\n",
    "from matplotlib import pyplot\n",
    "\n",
    "from sklearn.preprocessing import StandardScaler, OneHotEncoder\n",
    "from sklearn.compose import make_column_transformer\n",
    "from sklearn.model_selection import GroupShuffleSplit\n",
    "\n",
    "from tensorflow.keras import Sequential\n",
    "from tensorflow.keras.activations import relu\n",
    "from tensorflow.keras.layers import BatchNormalization, Input, Dense, Dropout\n",
    "from tensorflow.keras.losses import MeanAbsoluteError\n",
    "from tensorflow.keras.optimizers import Adam\n",
    "from tensorflow.keras.optimizers import SGD\n",
    "\n",
    "# noinspection PyUnresolvedReferences\n",
    "from tensorflow.keras.callbacks import EarlyStopping"
   ],
   "id": "42da68d737fe82ec",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Set Matplotlib defaults",
   "id": "7d6db2a597b4c455"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "pyplot.style.use(\"seaborn-v0_8-whitegrid\")\n",
    "pyplot.rc(\"figure\", autolayout=True)\n",
    "pyplot.rc(\n",
    "    \"axes\", labelweight=\"bold\", labelsize=\"large\",\n",
    "    titleweight=\"bold\", titlesize=18, titlepad=10\n",
    ")\n",
    "pyplot.rc(\"animation\", html=\"html5\")"
   ],
   "id": "f4606cc8f2b80c4e",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "import dataframes",
   "id": "989a38976b0c5e35"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": "spotify = read_csv(\"datasets/spotify.csv\")",
   "id": "f21de3f1501404a1",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "The purpose of the following code is to prepare a dataset for analysis or machine learning. Specifically:\n",
    "\n",
    "- It removes any rows with missing values to clean the data.\n",
    "- It separates the feature variables (inputs) from the target variable (output you want to predict), in this \n",
    "case, the \"track_popularity\".\n",
    "- It categorizes the features into numerical and categorical lists for easier processing or modeling later on."
   ],
   "id": "dc580b707d9faca7"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "training_dataframe = spotify.copy().dropna()\n",
    "target_dataframe = training_dataframe.pop(\"track_popularity\")\n",
    "artists = training_dataframe['track_artist']\n",
    "\n",
    "features_numerical = [\n",
    "    'danceability', 'energy', 'key', 'loudness', 'mode',\n",
    "    'speechiness', 'acousticness', 'instrumentalness',\n",
    "    'liveness', 'valence', 'tempo', 'duration_ms'\n",
    "]\n",
    "\n",
    "features_categorical = ['playlist_genre']"
   ],
   "id": "3fba74c2ff62d0c3",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Standardize numerical and categorical features",
   "id": "ca9fada03cb9c692"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "preprocessor = make_column_transformer(\n",
    "    (StandardScaler(), features_numerical),\n",
    "    (OneHotEncoder(), features_categorical)\n",
    ")"
   ],
   "id": "6e011573aeb4408b",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "The group_split function takes a dataset and splits it into training and testing sets, ensuring that samples \n",
    "from the same group are not split between the two sets.\n",
    "<br>  \n",
    "The function group_split takes four parameters:\n",
    "- training_dataframe_: The DataFrame containing the features.\n",
    "- target_dataframe_: The DataFrame containing the target values (labels).\n",
    "- group: A sequence (like a list or array) that indicates the group each sample belongs to.\n",
    "- train_size: Proportion of the dataset to include in the training set (default is 75%).\n",
    "\n",
    "The function returns a tuple containing:\n",
    "- The training set of the features DataFrame.\n",
    "- The testing set of the features DataFrame.\n",
    "- The training set of the target DataFrame.\n",
    "- The testing set of the target DataFrame."
   ],
   "id": "cf211303d25a7a58"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "def group_split(training_dataframe_, target_dataframe_, group, train_size=0.75):\n",
    "    splitter = GroupShuffleSplit(train_size=train_size)\n",
    "    train, test = next(splitter.split(training_dataframe_, target_dataframe_, groups=group))\n",
    "    \n",
    "    return (\n",
    "        training_dataframe_.iloc[train], training_dataframe_.iloc[test],\n",
    "        target_dataframe_.iloc[train], target_dataframe_.iloc[test]\n",
    "    )"
   ],
   "id": "c022fdebbfc1eab1",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "Split the dataset into a training set and a validation set using custom function\n",
    "`group_split`. Then, both training and validation sets are preprocessed.\n",
    "\n",
    "The fit_transform method fits the preprocessor to the data and then transforms it. This\n",
    " is used on the training set. The transform function uses the preprocessor fit on the \n",
    " training set to transform the validation set. This ensures that the same \n",
    " transformations are applied to both the training and validation sets.\n",
    " \n",
    "Finally, the target variables of the training and validation sets are divided by 100. \n",
    "This might be for scaling/normalization purposes, but without more information about \n",
    "what target_training_set and target_validation_set represent, it's hard to say \n",
    "definitively."
   ],
   "id": "c74b33a2c829580c"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "features_training_set, features_validation_set, \\\n",
    "    target_training_set, target_validation_set = group_split(\n",
    "    training_dataframe, target_dataframe, artists\n",
    ")\n",
    "\n",
    "features_training_set = preprocessor.fit_transform(features_training_set)\n",
    "features_validation_set = preprocessor.transform(features_validation_set)\n",
    "target_training_set = target_training_set / 100\n",
    "target_validation_set = target_validation_set / 100"
   ],
   "id": "c8160cf8a9bb1fff",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Set input shape as a list then display",
   "id": "b8557508de230814"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "input_shape = [features_training_set.shape[1]]\n",
    "print(\"Input shape: {}\".format(input_shape))"
   ],
   "id": "a46dade8c3b92e96",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Text",
   "id": "f3ab129059328d6"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "model = Sequential([\n",
    "    Input(shape=input_shape),\n",
    "    Dense(units=128, activation=relu),\n",
    "    Dropout(rate=0.3),\n",
    "    Dense(units=64, activation=relu),\n",
    "    Dropout(rate=0.3),\n",
    "    Dense(units=1)\n",
    "])\n",
    "\n",
    "model.compile(\n",
    "    optimizer=Adam(),\n",
    "    loss=MeanAbsoluteError()\n",
    ")\n",
    "\n",
    "history = model.fit(\n",
    "    features_training_set, target_training_set,\n",
    "    validation_data=(features_validation_set, target_validation_set),\n",
    "    batch_size=512,\n",
    "    epochs=50,\n",
    "    verbose=0   # suppress output since we'll plot the curves\n",
    ")"
   ],
   "id": "12e69819c096654f",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "This history object contains the training loss and accuracy, as well as the validation \n",
    "loss and accuracy if validation data was provided during model training.\n",
    "\n",
    " Then create a pandas DataFrame from the history object returned by the keras model \n",
    " training function (typically fit or fit_generator). This history object contains the \n",
    " training loss and accuracy, as well as the validation loss and accuracy if validation \n",
    " data was provided during model training.\n",
    " \n",
    " Plot the training and validation loss via matplotlib (which is implicitly used by \n",
    " pandas). history_dataframe.loc[:, [\"loss\", \"val_loss\"]] is indexing the DataFrame to \n",
    " select all rows (:) and only the \"loss\" and \"val_loss\" columns. The .plot() function \n",
    " is then called on this selection, which will create a line plot of training and \n",
    " validation loss over all epochs.\n",
    " \n",
    " Finally, calculating and printing the minimum validation loss is achieved during the \n",
    " training. {:0.4f} is a placeholder for a floating-point number with 4 digits after the\n",
    "  decimal point. The min function is called on the \"val_loss\" column of the DataFrame, \n",
    "  which will return the minimum value in that column. And this value will be placed at \n",
    "  the placeholder when format function is called."
   ],
   "id": "a0188a5c1551f5c1"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "history_dataframe = DataFrame(history.history)\n",
    "history_dataframe.loc[:, [\"loss\", \"val_loss\"]].plot()\n",
    "\n",
    "print(\"Minimum Validation Loss: {:0.4f}\".format(history_dataframe[\"val_loss\"].min()))"
   ],
   "id": "a3cae39ef3533461",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Load concrete dataset",
   "id": "4478e56737c0853e"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "concrete = read_csv(\"datasets/concrete.csv\")\n",
    "\n",
    "concrete_copy = concrete.copy()\n",
    "\n",
    "training_dataframe = concrete_copy.sample(frac=0.7, random_state=0)\n",
    "validation_dataframe = concrete_copy.drop(training_dataframe.index)\n",
    "\n",
    "features_training_set = training_dataframe.drop(\"CompressiveStrength\", axis=1)\n",
    "features_validation_set = validation_dataframe.drop(\"CompressiveStrength\", axis=1)\n",
    "target_training_set = training_dataframe[\"CompressiveStrength\"]\n",
    "target_validation_set = validation_dataframe[\"CompressiveStrength\"]\n",
    "\n",
    "input_shape = (features_training_set.shape[1],)"
   ],
   "id": "84eff12a671cce00",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "Run and train the network on the unstandardized concrete data.",
   "id": "c1854fb7391db19b"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "model = Sequential([\n",
    "    Input(shape=input_shape),\n",
    "    Dense(units=512, activation=relu),\n",
    "    Dense(units=512, activation=relu),\n",
    "    Dense(units=512, activation=relu),\n",
    "    Dense(units=1)\n",
    "])\n",
    "\n",
    "model.compile(\n",
    "    optimizer=SGD(),\n",
    "    loss=MeanAbsoluteError(),\n",
    "    metrics=[MeanAbsoluteError()]\n",
    ")\n",
    "\n",
    "history = model.fit(\n",
    "    features_training_set, target_training_set,\n",
    "    validation_data=(features_validation_set, target_validation_set),\n",
    "    batch_size=64,\n",
    "    epochs=100,\n",
    "    verbose=0\n",
    ")\n",
    "\n",
    "history_dataframe = DataFrame(history.history)\n",
    "history_dataframe.loc[0:, [\"loss\", \"val_loss\"]].plot()\n",
    "print(\"Minimum Validation Loss: {:0.4f}\".format(history_dataframe[\"val_loss\"].min()))"
   ],
   "id": "62c0ec3b939c177b",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "The previous will give a blank graph because it tends to converge to a very large\n",
    "number.\n",
    "\n",
    "We will add four `BatchNormalization` layers, one before each of the dense layers."
   ],
   "id": "1529d1878c8a5227"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "model = Sequential([\n",
    "    Input(shape=input_shape),\n",
    "    BatchNormalization(),\n",
    "    Dense(units=512, activation=relu),\n",
    "    BatchNormalization(),\n",
    "    Dense(units=512, activation=relu),\n",
    "    BatchNormalization(),\n",
    "    Dense(units=512, activation=relu),\n",
    "    BatchNormalization(),\n",
    "    Dense(units=1)\n",
    "])\n",
    "\n",
    "model.compile(\n",
    "    optimizer=SGD(),\n",
    "    loss=MeanAbsoluteError(),\n",
    "    metrics=[MeanAbsoluteError()]\n",
    ")\n",
    "\n",
    "history = model.fit(\n",
    "    features_training_set, target_training_set,\n",
    "    validation_data=(features_validation_set, target_validation_set),\n",
    "    batch_size=64,\n",
    "    epochs=100,\n",
    "    verbose=0\n",
    ")\n",
    "\n",
    "history_dataframe = DataFrame(history.history)\n",
    "history_dataframe.loc[0:, [\"loss\", \"val_loss\"]].plot()\n",
    "print(\"Minimum Validation Loss: {:0.4f}\".format(history_dataframe[\"val_loss\"].min()))"
   ],
   "id": "42421b3b2665ab81",
   "outputs": [],
   "execution_count": null
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
