{
 "cells": [
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "# A Single Neuron Exercise",
   "id": "622fe15964feef6e"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## Introduction\n",
    "\n",
    "In the tutorial we learned about the building blocks of neural networks: linear units. We saw that a model of just one linear unit will fit a linear function to a dataset (equivalent to linear regression). In this exercise, you'll build a linear model and get some practice working with models in Keras."
   ],
   "id": "9ca1cf42556760a1"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "from matplotlib import pyplot\n",
    "from pandas import read_csv\n",
    "import tensorflow\n",
    "from tensorflow import keras\n",
    "from tensorflow.keras import layers"
   ],
   "id": "10f3bac040179e84",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "# Set Matplot defaults\n",
    "pyplot.style.use(\"seaborn-v0_8-whitegrid\")\n",
    "pyplot.rc('figure', autolayout=True)\n",
    "pyplot.rc('axes', labelweight='bold', labelsize='large',\n",
    "                     titleweight='bold', titlesize=18, titlepad=10)"
   ],
   "id": "ea4ac1c1171ef80",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "The Red Wine Quality dataset consists of physiochemical measurements from about 1600 Portuguese red wines. Also included is a quality rating for each wine from blind taste-tests.\n",
    "\n",
    "First, run the next cell to display the first few rows of this dataset."
   ],
   "id": "4f93c0fbe898eca9"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "red_wine = read_csv(\"datasets/red-wine.csv\")\n",
    "red_wine.head()"
   ],
   "id": "8fd2d4bba2fae9df",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": "You can get the number of rows and columns of a dataframe (or a Numpy array) with the `shape` attribute.",
   "id": "e136b7732578b580"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": "red_wine.shape",
   "id": "90a3f7b3e69af2fc",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### Input shape\n",
    "\n",
    "How well can we predict a wine's perceived quality from the physiochemical measurements?\n",
    "\n",
    "The target is 'quality', and the remaining columns are the features. How would you set the input_shape parameter for a Keras model on this task?\n",
    "\n",
    "Hint: Remember to only count the input features when determining input_shape.\n",
    " You should not count the target (the quality column).\n"
   ],
   "id": "8f79767c47ba37f5"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": "input_shape = [11]",
   "id": "3c8b6fa2d8b2acfd",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### Define a linear model\n",
    "\n",
    "Now define a linear model appropriate for this task. Pay attention to how many inputs and outputs the model should have."
   ],
   "id": "61bed7b77c4b4342"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "model = keras.Sequential([\n",
    "    layers.Input(shape=[11]),\n",
    "    layers.Dense(units=1)\n",
    "])"
   ],
   "id": "2660f7bd9b3c7c05",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### Look at the weights\n",
    "\n",
    "Internally, Keras represents the weights of a neural network with tensors.\n",
    " Tensors are basically TensorFlow's version of a Numpy array with a few\n",
    "  differences that make them better suited to deep learning. One of the most\n",
    "   important is that tensors are compatible with GPU and TPU) accelerators.\n",
    "    TPUs, in fact, are designed specifically for tensor computations.\n",
    "\n",
    "A model's weights are kept in its weights attribute as a list of tensors.\n",
    " Get the weights of the model you defined above. (If you want, you could \n",
    " display the weights with something like: \n",
    " print(\"Weights\\n{}\\n\\nBias\\n{}\".format(w, b)))."
   ],
   "id": "a76bd4550d28d193"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "weights, bias = model.weights\n",
    "\n",
    "print(\"Weights\\n{}\\n\\nBias\\n{}\".format(weights, bias))"
   ],
   "id": "ca5a1ad8dff9b854",
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "### Plot the output of an untrained linear model\n",
    "\n",
    "The kinds of problems we'll work on through Lesson 5 will be regression \n",
    "problems, where the goal is to predict some numeric target. Regression \n",
    "problems are like \"curve-fitting\" problems: we're trying to find a curve \n",
    "that best fits the data. Let's take a look at the \"curve\" produced by a \n",
    "linear model. (You've probably guessed that it's a line!)\n",
    "\n",
    "We mentioned that before training a model's weights are set randomly. Run \n",
    "the cell below a few times to see the different lines produced with a random\n",
    " initialization. (There's no coding for this exercise -- it's just a \n",
    " demonstration.)"
   ],
   "id": "891c86c628eb6071"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "source": [
    "model = keras.Sequential([\n",
    "    layers.Input(shape=[1]),\n",
    "    layers.Dense(1)\n",
    "])\n",
    "\n",
    "x = tensorflow.linspace(-1.0, 1.0, 100)\n",
    "y = model.predict(x)\n",
    "\n",
    "pyplot.figure(dpi=100)\n",
    "pyplot.plot(x, y, 'k')\n",
    "pyplot.xlim(-1, 1)\n",
    "pyplot.ylim(-1, 1)\n",
    "pyplot.xlabel(\"Input: x\")\n",
    "pyplot.ylabel(\"Target y\")\n",
    "weights, bias = model.weights\n",
    "pyplot.title(\"Weight: {:0.2f}\\nBias: {:0.2f}\".format(weights[0][0], bias[0]))\n",
    "pyplot.show()"
   ],
   "id": "66fa612a255387cf",
   "outputs": [],
   "execution_count": null
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
